
/**
 * Implementation of disjoint sets partition
 * with union by rank and path compression.
 */

type tree('a) = {
  mutable parent: tree('a),
  mutable rank: int,
  value: 'a,
};

type t('a);

let makeNode(v) : tree('a) = {
  let rec n = {
    parent: n,
    rank: 0,
    value: v,
  };
  n;
};

let rec find(n) {
  if (n !== n.parent) {
    n.parent = find(n.parent);
  }
  n.parent;
};

let union(a, b) = {
  let aRoot = find(a);
  let bRoot = find(b);
  if (aRoot.rank > bRoot.rank) {
    bRoot.parent = aRoot;
  } else {
    aRoot.parent = bRoot;
    if (aRoot.rank == bRoot.rank) {
      bRoot.rank = bRoot.rank + 1;
    }
  }
};
