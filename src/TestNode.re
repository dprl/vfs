
let testReplace(subtree, replacement, context, target) = TestUtil.test(
  // () => Node.replaceNodeSubtree(subtree, replacement, context),
  // target -> (((tree, floating)) => (tree, floating -> Array.of_list))

  // // Testing Match replace == Node replace
  // () => Match.replaceSubtree(Some(subtree), Some(replacement), Some(context)),
  () => Node.replaceSubtree(Some(subtree), Some(replacement), Some(context)),
  target -> (((tree, floating)) => (Some(tree), floating -> Array.of_list))
);

let testSimpleReplace() = testReplace(
  Node.make("a", "a"),
  Node.make("b", "b"),
  Node.make("a", "a"),
  (
    Node.make("b", "b"),
    [],
  ),
);

let testReplace1() = testReplace(
  Node.make("a", "a"),
  Node.make("c", "c"),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  ( // 1
    Node.make("c", "c")
    |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
    []
  ),
);

let abc = Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c"))
  );

let abcd = Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c")
      |> Node.relate(Relations.SUB, Node.make("d", "d")
      )
    )
  );

let testReplace2() = testReplace(
  Node.make("a", "a"),
  Node.make("d", "d"),
  abc,
  ( // 2
    Node.make("d", "d")
    |> Node.relate(Relations.RIGHT, Node.make("b", "b")
      |> Node.relate(Relations.SUPER, Node.make("c", "c"))
    ),
    []
  ),
);

let t3 = ( // 3
    Node.make("a", "a")
    |> Node.relate(Relations.RIGHT, Node.make("b", "b")
      |> Node.relate(Relations.SUPER, Node.make("e", "e")
        |> Node.relate(Relations.SUB, Node.make("d", "d"))
      )
    ),
    []
  );
let testReplace3() = testReplace(
  {
    ...Node.make("c", "c"),
    rPath: [Relations.SUPER, Relations.RIGHT],
  },
  Node.make("e", "e"),
  abcd,
  t3
);

let testReplace4() = testReplace(
  {
    ...Node.make("c", "c"),
    rPath: [Relations.SUPER, Relations.RIGHT],
  },
  Node.make("e", "e")
    |> Node.relate(Relations.RIGHT, Node.make("f", "f")),
  abcd,
  ( // 4
    Node.make("a", "a")
    |> Node.relate(Relations.RIGHT, Node.make("b", "b")
      |> Node.relate(Relations.SUPER, Node.make("e", "e")
        |> Node.relate(Relations.RIGHT, Node.make("f", "f"))
        |> Node.relate(Relations.SUB, Node.make("d", "d"))
      )
    ),
    []
  ),
);

let testReplace5() = testReplace(
  {
    ...Node.make("c", "c"),
    rPath: [Relations.RIGHT, Relations.RIGHT],
  },
  Node.make("e", "e")
    |> Node.relate(Relations.RIGHT, Node.make("f", "f")),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.RIGHT, Node.make("c", "c")
      |> Node.relate(Relations.RIGHT, Node.make("d", "d")
      )
    )
  ),
  ( // 5
    Node.make("a", "a")
    |> Node.relate(Relations.RIGHT, Node.make("b", "b")
      |> Node.relate(Relations.RIGHT, Node.make("e", "e")
        |> Node.relate(Relations.RIGHT, Node.make("f", "f")
          |> Node.relate(Relations.RIGHT, Node.make("d", "d"))
        )
      )
    ),
    []
  ),
);

let testReplace6() = testReplace(
  {
    ...Node.make("c", "c"),
    rPath: [Relations.SUPER, Relations.RIGHT],
  },
  Node.make("e", "e"),
  abcd,
  t3,
);

let testReplace7() = testReplace(
  // replace
  {
    ...Node.make("b", "b"),
    rPath: [Relations.RIGHT],
  }
  |> Node.relate(Relations.SUPER, Node.make("c", "c")),
  // with
  Node.make("e", "e"),
  // in
  abcd,
  // results in
  (
    Node.make("a", "a")
    |> Node.relate(Relations.RIGHT, Node.make("e", "e")),
    [
      Node.make("d", "d"),
    ]
  )
);

let testReplace8() = testReplace(
  // replace
  {
    ...Node.make("b", "b"),
    rPath: [Relations.RIGHT],
  }
  |> Node.relate(Relations.SUPER, Node.make("c", "c")),
  // with
  Node.make("e", "e")
  |> Node.relate(Relations.SUB, Node.make("f", "f")),
  // in
  abcd,
  // results in
  (
    Node.make("a", "a")
    |> Node.relate(Relations.RIGHT, Node.make("e", "e")
      |> Node.relate(Relations.SUB, Node.make("f", "f"))
    ),
    [
      Node.make("d", "d"),
    ]
  )
);

let testReplace9() = testReplace(
  // replace
  {
    ...Node.make("b", "b"),
    rPath: [Relations.RIGHT],
  }
  |> Node.relate(Relations.SUPER, Node.make("c", "c")),
  // with
  Node.make("e", "e")
  |> Node.relate(Relations.SUPER, Node.make("f", "f")
    |> Node.relate(Relations.SUPER, Node.make("g", "g"))
    |> Node.relate(Relations.SUB, Node.make("h", "h"))
  ),
  // in
  abcd,
  // results in
  (
    Node.make("a", "a")
    |> Node.relate(Relations.RIGHT, Node.make("e", "e")
      |> Node.relate(Relations.SUPER, Node.make("f", "f")
        |> Node.relate(Relations.SUPER, Node.make("g", "g"))
        |> Node.relate(Relations.SUB, Node.make("h", "h")
          |> Node.relate(Relations.RIGHT, Node.make("d", "d"))
        )
      )
    ),
    []
  )
);

let testReplace10() = testReplace(
  abc,
  abc,
  abc,
  (
    abc,
    []
  )
);

let testReplace11() = testReplace(
  // replace
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c")),
  ),
  // with
  Node.make("f", "f")
  |> Node.relate(Relations.RIGHT, Node.make("g", "g")),
  // in
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c")
      |> Node.relate(Relations.SUPER, Node.make("d", "d"))
      |> Node.relate(Relations.SUB, Node.make("e", "e"))
    )
  ),
  // results in
  (
    Node.make("f", "f")
    |> Node.relate(Relations.RIGHT, Node.make("g", "g")
    ),
    [
      Node.make("d", "d"),
      Node.make("e", "e"),
    ]
  )
);


let runReplace() = {
  [
    testSimpleReplace,
    testReplace1,
    testReplace2,
    testReplace3,
    testReplace4,
    testReplace5,
    testReplace6,
    testReplace7,
    testReplace8,
    testReplace9,
    testReplace10,
    testReplace11,
  ]
  |> TestUtil.runTests("Node Replace");
};


// let testSubtrees(nodes, r) = TestUtil.testEq(
//   () => Node.getSubtrees(nodes),
//   (l1, l2) =>
//     l1 |> Node.NodeSet.of_list
//     |> Node.NodeSet.equal(
//     l2 |> Node.NodeSet.of_list),
//   r,
// );

// let testSubtrees0() = testSubtrees(
//   [
//     Node.make("a", "a")
//     |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
//     {
//       ...Node.make("b", "b"),
//       rPath: [Relations.RIGHT]
//     }
//   ],
//   [
//     Node.make("a", "a")
//     |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
//   ]
// );

// let testSubtrees1() = testSubtrees(
//   [
//     Node.make("a", "a"),
//     Node.make("b", "b"),
//   ],
//   [
//     Node.make("a", "a"),
//     Node.make("b", "b"),
//   ]
// );

// let testSubtrees2() = testSubtrees(
//   [],
//   []
// );

// let testSubtrees3() = testSubtrees(
//   [
//     Node.make("a", "a"),
//     Node.make("b", "b"),
//     Node.make("c", "c"),
//   ],
//   [
//     Node.make("a", "a"),
//     Node.make("b", "b"),
//     Node.make("c", "c"),
//   ]
// );

// let testSubtrees4() = testSubtrees(
//   abc
//   |> Node.mapList(n => n),
//   [
//     abc,
//   ]
// );

// let testSubtrees5() = testSubtrees(
//   abcd
//   |> Node.mapList(n => n)
//   |> List.filter((n : Node.t(string)) => n.id != Some("c")),
//   [
//     Node.make("a", "a")
//     |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
//     {
//      ...Node.make("d", "d"),
//       rPath: [Relations.SUB, Relations.SUPER, Relations.RIGHT]
//     },
//   ]
// );

// let testSubtrees6() = testSubtrees(
//   Node.make("a", "a")
//   |> Node.relate(Relations.RIGHT, Node.make("b", "b")
//     |> Node.relate(Relations.SUPER, Node.make("c", "c")
//       |> Node.relate(Relations.SUB, Node.make("d", "d"))
//       |> Node.relate(Relations.SUPER, Node.make("e", "e"))
//     )
//   )
//   |> Node.mapList(n => n)
//   |> List.filter((n : Node.t(string)) => n.id != Some("c")),
//   [
//     Node.make("a", "a")
//     |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
//     {
//      ...Node.make("d", "d"),
//       rPath: [Relations.SUB, Relations.SUPER, Relations.RIGHT]
//     },
//     {
//      ...Node.make("e", "e"),
//       rPath: [Relations.SUPER, Relations.SUPER, Relations.RIGHT]
//     }
//   ]
// );

// let runSubtrees() = {
//   [
//     testSubtrees0,
//     testSubtrees1,
//     testSubtrees2,
//     testSubtrees3,
//     testSubtrees4,
//     testSubtrees5,
//     testSubtrees6,
//   ]
//   |> TestUtil.runTests("Subtrees");
// };


let testMap(before, f, target) = TestUtil.test(
  () => before |> Node.map(f),
  target,
);

let testMap0() = testMap(
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c")
      |> Node.relate(Relations.SUB, Node.make("d", "d"))
      |> Node.relate(Relations.SUPER, Node.make("e", "e"))
    )
  ),
  n: Node.t(string, 'a) => {
    ...n,
    value: switch(n.value) {
      | None => None
      | Some(v) => Some(v ++ "+")
    },
  },
  Node.make("a", "a+")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b+")
    |> Node.relate(Relations.SUPER, Node.make("c", "c+")
      |> Node.relate(Relations.SUB, Node.make("d", "d+"))
      |> Node.relate(Relations.SUPER, Node.make("e", "e+"))
    )
  ),
);

let testMap1() = testMap(
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c")
      |> Node.relate(Relations.SUB, Node.make("d", "d"))
      |> Node.relate(Relations.SUPER, Node.make("e", "e"))
    )
  ),
  n => n |> Node.relate(Relations.ABOVE, Node.make("x", "x")),
  Node.make("a", "a")
  |> Node.relate(Relations.ABOVE, Node.make("x", "x"))
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.ABOVE, Node.make("x", "x"))
    |> Node.relate(Relations.SUPER, Node.make("c", "c")
      |> Node.relate(Relations.ABOVE, Node.make("x", "x"))
      |> Node.relate(Relations.SUB, Node.make("d", "d")
        |> Node.relate(Relations.ABOVE, Node.make("x", "x"))
      )
      |> Node.relate(Relations.SUPER, Node.make("e", "e")
        |> Node.relate(Relations.ABOVE, Node.make("x", "x"))
      )
    )
  ),
);

let runMap() = {
  [
    testMap0,
    testMap1,
  ]
  |> TestUtil.runTests("Map");
};


let testInsert(parent, r, child, context, target) = TestUtil.test(
  () => context |> Node.insert(parent, r, child) |> Node.string_of_node(v=>v),
  target |> Node.string_of_node(v=>v),
);
let testInsertBefore(parent, r, child, context, target) = TestUtil.test(
  () => context |> Node.insertBefore(parent, r, child) |> Node.string_of_node(v=>v),
  target |> Node.string_of_node(v=>v),
);


let testInsert0() = testInsert(
  Node.make("a", "a"),
  Relations.RIGHT,
  Node.make("b", "b"),
  Node.make("a", "a"),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
);

let testInsert1() = testInsert(
  {
    ...Node.make("b", "b"),
    rPath: [Relations.RIGHT],
  }
  |> Node.relate(Relations.SUPER, Node.make("c", "c")),
  Relations.RIGHT,
  Node.make("d", "d"),
  abc,
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c"))
    |> Node.relate(Relations.RIGHT, Node.make("d", "d"))
  ),
);

let testInsert2() = testInsert(
  abc
  |> Node.getById("b")
  |> Util.unsafe_some,
  Relations.RIGHT,
  Node.make("d", "d"),
  abc,
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c"))
    |> Node.relate(Relations.RIGHT, Node.make("d", "d"))
  ),
);

let testInsert3() = testInsert(
  abcd
  |> Node.getById("d")
  |> Util.unsafe_some,
  Relations.RIGHT,
  Node.make("e", "e"),
  abcd,
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c")
      |> Node.relate(Relations.SUB, Node.make("d", "d")
        |> Node.relate(Relations.RIGHT, Node.make("e", "e"))
      )
    )
  ),
);

let testInsert4() = testInsert(
  Node.make("a", "a"),
  Relations.RIGHT,
  Node.make("e", "e")
  |> Node.relate(Relations.RIGHT, Node.make("f", "f")),
  abcd,
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("e", "e")
    |> Node.relate(Relations.RIGHT, Node.make("f", "f")
      |> Node.relate(Relations.RIGHT, Node.make("b", "b")
        |> Node.relate(Relations.SUPER, Node.make("c", "c")
            |> Node.relate(Relations.SUB, Node.make("d", "d"))
        )
      )
    )
  ),
);

let runInsert() = {
  [
    testInsert0,
    testInsert1,
    testInsert2,
    testInsert3,
    testInsert4,
  ]
  |> TestUtil.runTests("Insert");
};


let abcdMat = Node.makeTyped("t", Node.TABLE, "")
  |> Node.relate(Relations.CONTAINS, Node.make("a", "a")
    |> Node.relate(Relations.COL, Node.make("b", "b"))
    |> Node.relate(Relations.ROW, Node.make("c", "c")
      |> Node.relate(Relations.COL, Node.make("d", "d"))
    )
  );


let testTableInsert0() = testInsertBefore(
  // at A
  Node.make("a", "a"),
  // with relation COL
  Relations.COL,
  // insert e
  Node.make("e", "e"),
  // in matrix
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, abcdMat),
  // resulting in
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.makeTyped("t", Node.TABLE, "")
    |> Node.relate(Relations.CONTAINS, Node.make("e", "e")
      |> Node.relate(Relations.COL, Node.make("a", "a")
        |> Node.relate(Relations.COL, Node.make("b", "b"))
      )
      |> Node.relate(Relations.ROW, Node.empty
        |> Node.relate(Relations.COL, Node.make("c", "c")
          |> Node.relate(Relations.COL, Node.make("d", "d"))
        )
      )
    )
  ),
);

let testTableInsert1() = testInsertBefore(
  // at B
  Node.make("b", "b"),
  // with relation COL
  Relations.COL,
  // insert e
  Node.make("e", "e"),
  // in matrix
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, abcdMat),
  // resulting in
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.makeTyped("t", Node.TABLE, "")
    |> Node.relate(Relations.CONTAINS, Node.make("a", "a")
      |> Node.relate(Relations.COL, Node.make("e", "e")
        |> Node.relate(Relations.COL, Node.make("b", "b"))
      )
      |> Node.relate(Relations.ROW, Node.make("c", "c")
        |> Node.relate(Relations.COL, Node.empty
          |> Node.relate(Relations.COL, Node.make("d", "d"))
        )
      )
    )
  ),
);

let testTableInsert2() = testInsertBefore(
  // at C
  Node.make("c", "c"),
  // with relation COL
  Relations.COL,
  // insert e
  Node.make("e", "e"),
  // in matrix
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, abcdMat),
  // resulting in
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.makeTyped("t", Node.TABLE, "")
    |> Node.relate(Relations.CONTAINS, Node.empty
      |> Node.relate(Relations.COL, Node.make("a", "a")
        |> Node.relate(Relations.COL, Node.make("b", "b"))
      )
      |> Node.relate(Relations.ROW, Node.make("e", "e")
        |> Node.relate(Relations.COL, Node.make("c", "c")
          |> Node.relate(Relations.COL, Node.make("d", "d"))
        )
      )
    )
  ),
);


let testTableInsert3() = testInsertBefore(
  // at D
  Node.make("d", "d"),
  // with relation COL
  Relations.COL,
  // insert e
  Node.make("e", "e"),
  // in matrix
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, abcdMat),
  // resulting in
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.makeTyped("t", Node.TABLE, "")
    |> Node.relate(Relations.CONTAINS, Node.make("a", "a")
      |> Node.relate(Relations.COL, Node.empty
        |> Node.relate(Relations.COL, Node.make("b", "b"))
      )
      |> Node.relate(Relations.ROW, Node.make("c", "c")
        |> Node.relate(Relations.COL, Node.make("e", "e")
          |> Node.relate(Relations.COL, Node.make("d", "d"))
        )
      )
    )
  ),
);

let testTableInsert4() = testInsertBefore(
  // at D
  Node.make("d", "d"),
  // with relation COL
  Relations.COL,
  // insert e f
  Node.make("e", "e")
  |> Node.relate(Relations.COL, Node.make("f", "f")),
  // in matrix
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, abcdMat),
  // resulting in
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.makeTyped("t", Node.TABLE, "")
    |> Node.relate(Relations.CONTAINS, Node.make("a", "a")
      |> Node.relate(Relations.COL, Node.empty
        |> Node.relate(Relations.COL, Node.empty
          |> Node.relate(Relations.COL, Node.make("b", "b"))
        )
      )
      |> Node.relate(Relations.ROW, Node.make("c", "c")
        |> Node.relate(Relations.COL, Node.make("e", "e")
          |> Node.relate(Relations.COL, Node.make("f", "f")
            |> Node.relate(Relations.COL, Node.make("d", "d"))
          )
        )
      )
    )
  ),
);

let testTableInsert5() = testInsertBefore(
  // at B
  Node.make("b", "b"),
  // with relation COL
  Relations.COL,
  // insert e f
  Node.make("e", "e")
  |> Node.relate(Relations.RIGHT, Node.make("f", "f")),
  // in matrix
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, abcdMat),
  // resulting in
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.makeTyped("t", Node.TABLE, "")
    |> Node.relate(Relations.CONTAINS, Node.make("a", "a")
      |> Node.relate(Relations.COL, Node.make("e", "e")
          |> Node.relate(Relations.RIGHT, Node.make("f", "f"))
        |> Node.relate(Relations.COL, Node.make("b", "b"))
      )
      |> Node.relate(Relations.ROW, Node.make("c", "c")
        |> Node.relate(Relations.COL, Node.empty
          |> Node.relate(Relations.COL, Node.make("d", "d"))
        )
      )
    )
  ),
);

let testTableInsert6() = testInsertBefore(
  // at B
  Node.make("b", "b"),
  // with relation ROW
  Relations.ROW,
  // insert e f
  Node.make("e", "e")
  |> Node.relate(Relations.RIGHT, Node.make("f", "f")),
  // in matrix
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, abcdMat),
  // resulting in
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.makeTyped("t", Node.TABLE, "")
    |> Node.relate(Relations.CONTAINS, Node.empty
      |> Node.relate(Relations.COL, Node.make("e", "e")
        |> Node.relate(Relations.RIGHT, Node.make("f", "f")))
      |> Node.relate(Relations.ROW, Node.make("a", "a")
        |> Node.relate(Relations.COL, Node.make("b", "b"))
        |> Node.relate(Relations.ROW, Node.make("c", "c")
          |> Node.relate(Relations.COL, Node.make("d", "d"))
        )
      )
    )
  ),
);

let testTableInsert7() = testInsertBefore(
  // at C
  Node.make("c", "c"),
  // with relation ROW
  Relations.ROW,
  // insert e
  Node.make("e", "e")
  |> Node.relate(Relations.RIGHT, Node.make("f", "f")),
  // in matrix
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, abcdMat),
  // resulting in
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.makeTyped("t", Node.TABLE, "")
    |> Node.relate(Relations.CONTAINS, Node.make("a", "a")
      |> Node.relate(Relations.COL, Node.make("b", "b"))
      |> Node.relate(Relations.ROW, Node.make("e", "e")
        |> Node.relate(Relations.RIGHT, Node.make("f", "f"))
        |> Node.relate(Relations.COL, Node.empty)
        |> Node.relate(Relations.ROW, Node.make("c", "c")
          |> Node.relate(Relations.COL, Node.make("d", "d"))
        )
      )
    )
  ),
);

let testTableInsert8() = testInsertBefore(
  // at A
  Node.make("a", "a"),
  // with relation ROW
  Relations.ROW,
  // insert e
  Node.make("e", "e")
  |> Node.relate(Relations.RIGHT, Node.make("f", "f")),
  // in matrix
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, abcdMat),
  // resulting in
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.makeTyped("t", Node.TABLE, "")
    |> Node.relate(Relations.CONTAINS, Node.make("e", "e")
      |> Node.relate(Relations.RIGHT, Node.make("f", "f"))
      |> Node.relate(Relations.COL, Node.empty)
      |> Node.relate(Relations.ROW, Node.make("a", "a")
        |> Node.relate(Relations.COL, Node.make("b", "b"))
        |> Node.relate(Relations.ROW, Node.make("c", "c")
          |> Node.relate(Relations.COL, Node.make("d", "d"))
        )
      )
    )
  ),
);

let testTableInsert9() = testInsertBefore(
  // at A
  Node.make("a", "a"),
  // with relation ROW
  Relations.ROW,
  // insert e
  Node.make("e", "e")
  |> Node.relate(Relations.ROW, Node.make("f", "f")),
  // in matrix
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, abcdMat),
  // resulting in
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.makeTyped("t", Node.TABLE, "")
    |> Node.relate(Relations.CONTAINS, Node.make("e", "e")
      |> Node.relate(Relations.COL, Node.empty)
      |> Node.relate(Relations.ROW, Node.make("f", "f")
        |> Node.relate(Relations.COL, Node.empty)
        |> Node.relate(Relations.ROW, Node.make("a", "a")
          |> Node.relate(Relations.COL, Node.make("b", "b"))
          |> Node.relate(Relations.ROW, Node.make("c", "c")
            |> Node.relate(Relations.COL, Node.make("d", "d"))
          )
        )
      )
    )
  ),
);

let testTableInsert10() = testInsertBefore(
  // at A
  Node.make("a", "a"),
  // with relation ROW
  Relations.ROW,
  // insert 2x2 efgh
  Node.make("e", "e")
  |> Node.relate(Relations.COL, Node.make("f", "f"))
  |> Node.relate(Relations.ROW, Node.make("g", "g")
    |> Node.relate(Relations.COL, Node.make("h", "h"))
  ),
  // in matrix
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, abcdMat),
  // resulting in
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.makeTyped("t", Node.TABLE, "")
    |> Node.relate(Relations.CONTAINS, Node.make("e", "e")
      |> Node.relate(Relations.COL, Node.make("f", "f"))
      |> Node.relate(Relations.ROW, Node.make("g", "g")
        |> Node.relate(Relations.COL, Node.make("h", "h"))
        |> Node.relate(Relations.ROW, Node.make("a", "a")
          |> Node.relate(Relations.COL, Node.make("b", "b"))
          |> Node.relate(Relations.ROW, Node.make("c", "c")
            |> Node.relate(Relations.COL, Node.make("d", "d"))
          )
        )
      )
    )
  ),
);


let runTableInsert() = {
  [
    testTableInsert0,
    testTableInsert1,
    testTableInsert2,
    testTableInsert3,
    testTableInsert4,
    testTableInsert5,
    testTableInsert6,
    testTableInsert7,
    testTableInsert8,
    testTableInsert9,
    testTableInsert10,
  ]
  |> TestUtil.runTests("Table Insert");
};


let testToString(node, soa, target) = TestUtil.test(
  () => node |> Node.string_of_node(soa),
  target,
);

let testToString0() = testToString(
  abc,
  v => v,
  "a b^{c}"
);

let testToString1() = testToString(
  abcd,
  v => v,
  "a b^{c_{d}}"
);

let testToString2() = testToString(
  Node.make("a", "a"),
  v => v,
  "a"
);

let testToString3() = testToString(
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.make("=", "=")
    |> Node.relate(Relations.RIGHT, Node.make("a", "a")
      |> Node.relate(Relations.ABOVE, Node.make("^", "^"))
    )
  ),
  v => v,
  "y = \\overset{^}{a}"
);

let testToString4() = testToString(
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.make("=", "=")
    |> Node.relate(Relations.RIGHT, Node.makeTyped("f", Node.FRAC, "")
      |> Node.relate(Relations.ABOVE, Node.make("a", "a"))
      |> Node.relate(Relations.BELOW, Node.make("b", "b"))
    )
  ),
  v => v,
  "y = \\frac{a}{b}"
);

let testToString5() = testToString(
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.make("=", "=")
    |> Node.relate(Relations.RIGHT, Node.makeTyped("f", Node.FRAC, "")
      |> Node.relate(Relations.ABOVE, Node.make("a", "a"))
      |> Node.relate(Relations.BELOW, Node.make("b", "b"))
      |> Node.relate(Relations.SUPER, Node.make("c", "c"))
      |> Node.relate(Relations.SUB, Node.make("d", "d"))
    )
  ),
  v => v,
  "y = \\frac{a}{b}^{c}_{d}"
);

let testToString6() = testToString(
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.make("=", "=")
    |> Node.relate(Relations.RIGHT, Node.makeTyped("s", Node.ROOT, "")
      |> Node.relate(Relations.CONTAINS, Node.make("a", "a"))
      |> Node.relate(Relations.SUPER, Node.make("c", "c"))
      |> Node.relate(Relations.SUB, Node.make("d", "d"))
    )
  ),
  v => v,
  "y = \\sqrt{a}^{c}_{d}"
);

let testToString7() = testToString(
  Node.make("y", "y")
  |> Node.relate(Relations.RIGHT, Node.make("=", "=")
    |> Node.relate(Relations.RIGHT, Node.makeTyped("r", Node.ROOT, "")
      |> Node.relate(Relations.CONTAINS, Node.make("a", "a"))
      |> Node.relate(Relations.PRESUPER, Node.make("3", "3"))
      |> Node.relate(Relations.SUPER, Node.make("c", "c"))
      |> Node.relate(Relations.SUB, Node.make("d", "d"))
    )
  ),
  v => v,
  "y = \\sqrt[3]{a}^{c}_{d}"
);

let testToString8() = testToString(
  Node.makeTyped("t", Node.TABLE, "")
  |> Node.relate(Relations.CONTAINS, Node.make("a", "a")
    |> Node.relate(Relations.COL, Node.make("b", "b"))
    |> Node.relate(Relations.ROW, Node.make("c", "c")
      |> Node.relate(Relations.COL, Node.make("d", "d"))
    )
  ),
  v => v,
  "\\begin{matrix}\na & b \\\\\nc & d\n\\end{matrix}"
);

let runToString() = {
  [
    testToString0,
    testToString1,
    testToString2,
    testToString3,
    testToString4,
    testToString5,
    testToString6,
    testToString7,
    testToString8,
  ]
  |> TestUtil.runTests("ToString");
};


let testInsertBefore0() = testInsertBefore(
  Node.make("a", "a"),
  Relations.RIGHT,
  Node.make("b", "b"),
  Node.make("a", "a"),
  Node.make("b", "b")
  |> Node.relate(Relations.RIGHT, Node.make("a", "a")),
);

let testInsertBefore1() = testInsertBefore(
  abc
  |> Node.getById("b")
  |> Util.unsafe_some,
  Relations.RIGHT,
  Node.make("d", "d"),
  abc,
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("d", "d")
    |> Node.relate(Relations.RIGHT, Node.make("b", "b")
      |> Node.relate(Relations.SUPER, Node.make("c", "c"))
    )
  ),
);

let testInsertBefore2() = testInsertBefore(
  abc
  |> Node.getById("b")
  |> Util.unsafe_some,
  Relations.RIGHT,
  Node.make("d", "d")
  |> Node.relate(Relations.RIGHT, Node.make("e", "e")
    |> Node.relate(Relations.SUPER, Node.make("f", "f"))
  ),
  abc,
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("d", "d")
    |> Node.relate(Relations.RIGHT, Node.make("e", "e")
      |> Node.relate(Relations.SUPER, Node.make("f", "f"))
      |> Node.relate(Relations.RIGHT, Node.make("b", "b")
        |> Node.relate(Relations.SUPER, Node.make("c", "c"))
      )
    )
  ),
);

let testInsertBefore3() = testInsertBefore(
  // before c in a^c b
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("c", "c"))
  |> Node.relate(Relations.RIGHT, Node.make("b", "b"))
  |> Node.getById("c")
  |> Util.unsafe_some,

  // insert d before RIGHT relation
  Relations.RIGHT,
  Node.make("d", "d"),

  // in a^c b
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("c", "c"))
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),

  // to get a^{dc} b
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("d", "d")
    |> Node.relate(Relations.RIGHT, Node.make("c", "c"))
  )
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
);


let runInsertBefore() = {
  [
    testInsertBefore0,
    testInsertBefore1,
    testInsertBefore2,
    testInsertBefore3,
  ]
  |> TestUtil.runTests("InsertBefore");
};

let testDelete(subtree, context, target) = TestUtil.test(
  () => context |> Node.deleteSubtree(subtree),
  target,
);

let testDelete0() = testDelete(
  Node.make("a", "a"),
  Node.make("a", "a"),
  (
    None,
    [||]
  )
);

// Replace/Delete assumes selection exists in context so this fails
let testDelete1() = testDelete(
  Node.make("a", "a"),
  Node.make("b", "b"),
  (
    Some(Node.make("b", "b")),
    [||]
  )
);

let testDelete2() = testDelete(
  Node.make("a", "a"),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  (
    Some(Node.make("b", "b")),
    [||]
  )
);

let testDelete3() = testDelete(
  {
    ...Node.make("b", "b"),
    rPath: [Relations.RIGHT]
  },
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  (
    Some(Node.make("a", "a")),
    [||]
  )
);

let testDelete4() = testDelete(
  {
    ...Node.make("b", "b"),
    rPath: [Relations.RIGHT]
  },
  abc,
  (
    Some(Node.make("a", "a")),
    [|
      Node.make("c", "c"),
    |]
  )
);

// This should pass with a delete* operation with *= path intersection
let testDeleteStar0() = testDelete(
  Node.make("a", "a"),
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  (
    Some(Node.make("b", "b")),
    [||]
  )
);

let testDeleteStar1() = testDelete(
  {
    ...Node.make("b", "b"),
    rPath: [Relations.RIGHT]
  },
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.RIGHT, Node.make("c", "c"))
  ),
  (
    Some(
      Node.make("a", "a")
      |> Node.relate(Relations.RIGHT, Node.make("c", "c"))
    ),
    [||]
  )
);

let testDeleteStar2() = testDelete(
  {
    ...Node.make("b", "b"),
    rPath: [Relations.SUPER]
  },
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("b", "b")
    |> Node.relate(Relations.RIGHT, Node.make("c", "c"))
  ),
  (
    Some(
      Node.make("a", "a")
      |> Node.relate(Relations.SUPER, Node.make("c", "c"))
    ),
    [||]
  )
);

let testDeleteStar3() = testDelete(
  {
    ...Node.make("b", "b"),
    rPath: [Relations.SUPER]
  },
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("b", "b")
    |> Node.relate(Relations.RIGHT, Node.make("c", "c")
      |> Node.relate(Relations.RIGHT, Node.make("d", "d"))
    )
  ),
  (
    Some(
      Node.make("a", "a")
      |> Node.relate(Relations.SUPER, Node.make("c", "c")
        |> Node.relate(Relations.RIGHT, Node.make("d", "d"))
      )
    ),
    [||]
  )
);

let testDeleteStar4() = testDelete(
  {
    ...Node.make("b", "b"),
    rPath: [Relations.SUPER]
  }
  |> Node.relate(Relations.SUPER, Node.make("e", "e")),
  Node.make("a", "a")
  |> Node.relate(Relations.SUPER, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("e", "e")
      |> Node.relate(Relations.RIGHT, Node.make("f", "f"))
    )
    |> Node.relate(Relations.RIGHT, Node.make("c", "c")
      |> Node.relate(Relations.RIGHT, Node.make("d", "d"))
    )
  ),
  (
    Some(
      Node.make("a", "a")
      |> Node.relate(Relations.SUPER, Node.make("c", "c")
        |> Node.relate(Relations.RIGHT, Node.make("d", "d"))
      )
    ),
    [|
      Node.make("f", "f"),
    |]
  )
);



let runDelete() = {
  [
    testDelete0,
    testDelete1,
    testDelete2,
    testDelete3,
    testDelete4,
    testDeleteStar0,
    testDeleteStar1,
    testDeleteStar2,
    testDeleteStar3,
    testDeleteStar4,
  ]
  |> TestUtil.runTests("Delete");
};

let testGroup(subtree, l, r, context, target) = TestUtil.test(
  () => Node.group(subtree, l, r, context),
  target -> (((tree, floating)) => (Some(tree), floating -> Array.of_list))
);

let testGroup0() = testGroup(
  Node.make("a", "a"),
  Some(Node.make("(", "(")),
  Some(Node.make(")", ")")),
  Node.make("a", "a"),
  (
    Node.make("(", "(")
    |> Node.relate(Relations.RIGHT, Node.empty
      |> Node.relate(Relations.CONTAINS, Node.make("a", "a"))
      |> Node.relate(Relations.RIGHT, Node.make(")", ")"))
    ),
    []
  )
);

let runGroup() = {
  [
    testGroup0,
  ]
  |> TestUtil.runTests("Group");
};


let run() = {
  runReplace();
  // runSubtrees();
  runMap();
  runInsert();
  runTableInsert();
  runToString();
  runInsertBefore();
  runDelete();
  runGroup();
};