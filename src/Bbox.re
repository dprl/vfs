
  type coord = { x:float, y:float };
  type size = { width: float, height: float };
  type bbox = { loc: coord, size: size };
  let origin = { x: 0., y: 0. };
  let emptySize = { width: 0., height: 0. };
  let emptyBbox = { loc: origin, size: emptySize };

  let makeCoord(x, y) = { x, y };
  let makeSize(width, height) = { width, height };

  let make(x, y, width, height) = {
    loc: makeCoord(x, y),
    size: makeSize(width, height),
  };

  let equals(b1, b2) = b1.loc == b2.loc && b1.size == b2.size;

  let left(b) = b.loc.x;
  let right(b) = b.loc.x +. b.size.width;
  let top(b) = b.loc.y;
  let bottom(b) = b.loc.y +. b.size.height;
  let isLeft(a, b) = a->right < b->left;
  let isRight(a, b) = a->left > b->right;
  let isAbove(a, b) = a->bottom < b->top;
  let isBelow(a, b) = a->top > b->bottom;
  let centerX(b) = b.loc.x +. b.size.width /. 2.;
  let centerY(b) = b.loc.y +. b.size.height /. 2.
  let center(b) = { x: b->centerX, y: b->centerY };
  let pBox(p) = { loc: p, size: { width: 0., height: 0. }}
  let intersectX(a, b) = !(a->isLeft(b) || a->isRight(b));
  let intersectY(a, b) = !(a->isAbove(b) || a->isBelow(b));
  let intersect(a, b) = a->intersectX(b) && a->intersectY(b);
  let insideX(p:coord, b) = p->pBox->intersectX(b);
  let insideY(p:coord, b) = p->pBox->intersectY(b);
  let inside(p:coord, b) = p->insideX(b) && p->insideY(b);
  let intersectMiddle(a, b) = a->center->inside(b) || b->center->inside(a);
  type range = { min: float, max: float };
  let makeRange(min, max) = { min, max };
  let clip(i, range) = max(min(i, range.max), range.min);
  let diameterOfRange(r) = r.max -. r.min;
  let radiusOfRange(r) = r->diameterOfRange /. 2.;
  let xRangeOfBox(b) = makeRange(b->left, b->right);
  let yRangeOfBox(b) = makeRange(b->top, b->bottom);
  let distXToCenter(p:coord, b) = abs_float(p.x -. b->centerX);
  let distYToCenter(p:coord, b) = abs_float(p.y -. b->centerY);
  let distX(p:coord, b) = p->distXToCenter(b) -. b->xRangeOfBox->radiusOfRange;
  let distY(p:coord, b) = p->distYToCenter(b) -. b->yRangeOfBox->radiusOfRange;
  let dist(a:coord, b:coord) = sqrt((b.x -. a.x) ** 2. +. (b.y -. a.y) ** 2.);
  let boxDist(p:coord, b) = sqrt(p->distX(b) ** 2. +. p->distY(b) ** 2.);
  let distL1(p:coord, b) = p->distX(b) +. p->distY(b);
  let distToBox(p:coord, b) = switch(p->insideX(b), p->insideY(b)){
    | (true, true) => 0.
    | (true, false) => p->distY(b)
    | (false, true) => p->distX(b)
    | (false, false) => p->boxDist(b)
  };

  let avg(a:coord, b:coord) = {
    x: (b.x +. a.x) /. 2.,
    y: (b.y +. a.y) /. 2.
  };
  let area(a: bbox) = a.size.width *. a.size.height;

  let topLeft(b) = { x: b->left, y: b->top };
  let topCenter(b) = { x: b->centerX, y: b->top };
  let topRight(b) = { x: b->right, y: b->top };
  let midLeft(b) = { x: b->left, y: b->centerY };
  let midCenter(b) = { x: b->centerX, y: b->centerY };
  let midRight(b) = { x: b->right, y: b->centerY };
  let botLeft(b) = { x: b->left, y: b->bottom };
  let botCenter(b) = { x: b->centerX, y: b->bottom };
  let botRight(b) = { x: b->right, y: b->bottom };

  let minIn(values) = switch(values) {
    | [] => 0.
    | [hd, ...tl] => tl
      |> List.fold_left((m, v) => v < m ? v : m, hd)
  };
  let maxIn(values) = switch(values) {
    | [] => 0.
    | [hd, ...tl] => tl
      |> List.fold_left((m, v) => v > m ? v : m, hd)
  };
  let rangeOf(values) = switch (values) {
  | [] => { min: 0., max: 0. }
  | [hd, ...tl] => tl
      |> List.fold_left(
        ((mn, mx), v) => (
          v < mn ? v : mn,
          v > mx ? v : mx,
        ), (hd, hd))
      |> ((mn, mx)) => { min: mn, max: mx }
  };

  let boxesRanges(boxes) = boxes
    |> List.fold_left(
      ((xs, ys), box) => (
        [box->left, box->right, ...xs],
        [box->top, box->bottom, ...ys],
      ),
      ([], []))
    |> ((xs, ys)) => (xs->rangeOf, ys->rangeOf);

  let boxOfRanges(xRange, yRange) = {
    loc: {
      x: xRange.min,
      y: yRange.min,
    },
    size: {
      width: xRange->diameterOfRange,
      height: yRange->diameterOfRange,
    },
  };

  let coordinatesBox(coords) = {
    let (xRange, yRange) = coords
    |> Array.fold_left(
      ((xs, ys), coord) => (
        [coord.x, ...xs],
        [coord.y, ...ys],
      ),
      ([], []))
    |> ((xs, ys)) => (xs->rangeOf, ys->rangeOf);
    boxOfRanges(xRange, yRange);
  };

  let boxesBox(boxes) = {
    let (xRange, yRange) = boxes->Array.to_list->boxesRanges;
    boxOfRanges(xRange, yRange);
  };
