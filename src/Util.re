
/**
  * perform f on value of x if x is not None
  * return Some(result) if so
  */
let ifSome(f, x) = switch(x) {
  | None => None
  | Some(xv) => Some(f(xv))
};

// remove a layer from an nested option
let unwrap(o: option(option('a))) = switch o {
  | None => None
  | Some(v) => v
};

let unsafe_some(opt: option('a)) : 'a = switch(opt) {
  | None => raise(Not_found)
  | Some(v) => v;
};
