
type n('a, 'b) = Node.t('a, 'b);

type t('a, 'b) = {
  eq: list(n('a, 'b)),
  minus: list(n('a, 'b)),
  plus: list(n('a, 'b)),
  delta: list((n('a, 'b), n('a, 'b))),
};

let empty = {
  eq: [],
  minus: [],
  plus: [],
  delta: [],
};

let diff(a: n('a, 'b), b: n('a, 'b)) = {
  Node.fold2(
    (acc, na_opt, nb_opt) => switch(na_opt, nb_opt) {
      | (Some(na), Some(nb)) => na.value == nb.value
        ? { ...acc, eq: [na->Node.getRoot, ...acc.eq] }
        : { ...acc, delta: [(na->Node.getRoot, nb->Node.getRoot), ...acc.delta] }
      | (Some(na), None) => { ...acc, minus: [na->Node.getRoot, ...acc.minus] }
      | (None, Some(nb)) => { ...acc, plus: [nb->Node.getRoot, ...acc.plus] }
      | (None, None) => acc
    },
    empty,
    a,
    b,
  );
};

let listToString(to_str, l) = l
  |> List.map(to_str)
  |> String.concat(", ");

let toString(s_of_a, diff: t('a, 'b)) = {
  let s_of_n = (n: n('a, 'b)) => n.value->s_of_a;
  let s_of_n_pair = ((n1: n('a, 'b), n2: n('a, 'b))) => "("
    ++ n1->s_of_n
    ++ " -> "
    ++ n2->s_of_n
    ++ ")";
  [
    "Diff:",
    "  Equal[ " ++ (diff.eq |> listToString(s_of_n)) ++ " ]",
    "  Minus[ " ++ (diff.minus |> listToString(s_of_n)) ++ " ]",
    "  Plus[ " ++ (diff.plus |> listToString(s_of_n)) ++ " ]",
    "  Delta[ " ++ (diff.delta |> listToString(s_of_n_pair)) ++ " ]",
  ] |> String.concat("\n");
};