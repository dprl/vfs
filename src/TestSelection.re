

let testRect(a, b, r) = TestUtil.test(
  () => Selection.getRect(a, b),
  r,
);

let rects : array(Bbox.bbox) = [|
  { // 0
    loc: { x: 0., y: 0. },
    size: { width: 1., height: 1. },
  },
  { // 1
    loc: { x: 10., y: 20. },
    size: { width: 20., height: 10. },
  },
  { // 2
    loc: { x: -30., y: -30. },
    size: { width: 20., height: 10. },
  },
  { // 3
    loc: { x: -30., y: -20. },
    size: { width: 40., height: 50. },
  },
|];

let testRect0() = testRect(
  { x: 0., y: 0. },
  { x: 1., y: 1. },
  rects[0],
);

let testRect1() = testRect(
  { x: 10., y: 20. },
  { x: 30., y: 30. },
  rects[1],
);

let testRect2() = testRect(
  { x: 30., y: 30. },
  { x: 10., y: 20. },
  rects[1],
);

let testRect3() = testRect(
  { x: -30., y: -30. },
  { x: -10., y: -20. },
  rects[2],
);

let testRect4() = testRect(
  { x: -30., y: 30. },
  { x: 10., y: -20. },
  rects[3],
);

let testRect5() = testRect(
  { x: 0., y: 0. },
  { x: 0., y: 0. },
  Bbox.make(0., 0., 0., 0.),
);

let runRect() = {
  [
    testRect0,
    testRect1,
    testRect2,
    testRect3,
    testRect4,
    testRect5,
  ]
  |> TestUtil.runTests("Rect");
};


let testValuesInBox(box, values, r) = TestUtil.test(
  () => Selection.getValuesInBox(box, box=>box, values),
  r,
);

let testValuesInBox0() = testValuesInBox(
  Bbox.make(0., 0., 0., 0.),
  [],
  []
);

let testValuesInBox1() = testValuesInBox(
  Bbox.make(0., 0., 0., 0.),
  [
    Bbox.make(0., 0., 0., 0.),
    Bbox.make(1., 1., 0., 0.),
    Bbox.make(0., 0., 1., 1.),
  ],
  [
    Bbox.make(0., 0., 0., 0.),
    Bbox.make(0., 0., 1., 1.),
  ]
);

let testValuesInBox2() = testValuesInBox(
  Bbox.make(10., 10., 20., 30.),
  [
    Bbox.make(0., 0., 0., 0.),
    Bbox.make(1., 1., 0., 0.),
    Bbox.make(0., 0., 1., 1.),
  ],
  []
);

let testValuesInBox3() = testValuesInBox(
  Bbox.make(10., 10., 20., 20.),
  [
    Bbox.make(0., 0., 30., 30.),
    Bbox.make(10., 10., 0., 0.),
    Bbox.make(0., 0., 10., 10.),
  ],
  [
    Bbox.make(0., 0., 30., 30.),
    Bbox.make(10., 10., 0., 0.),
    Bbox.make(0., 0., 10., 10.),
  ],
);

let testValuesInBox4() = testValuesInBox(
  Bbox.make(10., 10., 20., 20.),
  [
    Bbox.make(9., 10., 1., 0.),
    Bbox.make(0., 1., 10., 9.),
  ],
  [
    Bbox.make(9., 10., 1., 0.),
    Bbox.make(0., 1., 10., 9.),
  ],
);

let testValuesInBox5() = testValuesInBox(
  Bbox.make(10., 10., 20., 20.),
  [
    Bbox.make(10., 0., 10., 0.),
    Bbox.make(0., 10., 0., 10.),
  ],
  [],
);

let runInBox() = {
  [
    testValuesInBox0,
    testValuesInBox1,
    testValuesInBox2,
    testValuesInBox3,
    testValuesInBox4,
  ]
  |> TestUtil.runTests("In Box");
};


let testClosest(pt, boxes, r) = TestUtil.test(
  () => Selection.getClosest(pt, box=>box, boxes),
  r,
);

let testClosest0() = testClosest(
  Bbox.makeCoord(10., 10.),
  [
    Bbox.make(10., 0., 10., 0.),
    Bbox.make(0., 10., 0., 10.),
  ],
  Some(
    Bbox.make(10., 0., 10., 0.),
  )
);

let testClosest1() = testClosest(
  Bbox.makeCoord(10., 10.),
  [],
  None
);

let testClosest2() = testClosest(
  Bbox.makeCoord(10., 10.),
  [
    Bbox.make(12., 10., 20., 20.),
    Bbox.make(10., 12., 20., 20.),
    Bbox.make(11., 11., 20., 20.),
  ],
  Some(
    Bbox.make(11., 11., 20., 20.),
  )
);

let testClosest3() = testClosest(
  Bbox.makeCoord(33., 33.),
  [
    Bbox.make(12., 10., 20., 20.),
    Bbox.make(10., 12., 20., 20.),
    Bbox.make(11., 11., 20., 20.),
  ],
  Some(
    Bbox.make(11., 11., 20., 20.),
  )
);

let testClosest4() = testClosest(
  Bbox.makeCoord(30., 33.),
  [
    Bbox.make(12., 10., 20., 20.),
    Bbox.make(10., 12., 20., 20.),
    Bbox.make(11., 11., 20., 20.),
  ],
  Some(
    Bbox.make(10., 12., 20., 20.),
  )
);

let testClosest5() = testClosest(
  Bbox.makeCoord(33., 30.),
  [
    Bbox.make(12., 10., 20., 20.),
    Bbox.make(10., 12., 20., 20.),
    Bbox.make(11., 11., 20., 20.),
  ],
  Some(
    Bbox.make(12., 10., 20., 20.),
  )
);

let runClosest() = {
  [
    testClosest0,
    testClosest1,
    testClosest2,
    testClosest3,
    testClosest4,
    testClosest5,
  ]
  |> TestUtil.runTests("Closest");
};


let testSelectionSubtrees(box, getBox, context, r) = TestUtil.test(
  () => context
    |> Selection.getSelectionTree(box, getBox)
    |> Selection.getSelectionSubtrees,
  r,
);

let getBox(n : Node.t('a, 'b)) = switch(n.id) {
  | None => Bbox.make(0., 0., 0., 0.)
  | Some(id) => switch(id) {
    | "a" => Bbox.make(0., 0., 10., 10.)
    | "b" => Bbox.make(20., 0., 10., 10.)
    | "c" => Bbox.make(30., 0., 10., 10.)
    | _ => Bbox.make(0., 0., 0., 0.)
  }
};

let testSelectionSubtrees0() = testSelectionSubtrees(
  Bbox.make(0., 0., 15., 15.),
  getBox,
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c"))
  ),
  [
    Node.make("a", "a"),
  ]
);

let testSelectionSubtrees1() = testSelectionSubtrees(
  Bbox.make(0., 0., 25., 25.),
  getBox,
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c"))
  ),
  [
    Node.make("a", "a")
    |> Node.relate(Relations.RIGHT, Node.make("b", "b")),
  ]
);

let runSelectionSubtrees() = {
  [
    testSelectionSubtrees0,
    testSelectionSubtrees1,
  ]
  |> TestUtil.runTests("Selection Subtrees");
};


let testSelection(pA, pB, getBox, context, r) = TestUtil.test(
  () => Selection.getSelection(pA, pB, 0., getBox, context),
  r,
);

let abc = Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c"))
  );

let abcd = Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c")
      |> Node.relate(Relations.SUPER, Node.make("d", "d"))
    )
  );

let testSelection0() = testSelection(
  Bbox.makeCoord(0., 0.),
  Bbox.makeCoord(15., 15.),
  getBox,
  abc,
  (
    Some(Node.make("a", "a")),
    [||],
  )
);

let testSelection1() = testSelection(
  Bbox.makeCoord(20., 0.),
  Bbox.makeCoord(25., 15.),
  getBox,
  abc,
  (
    Some({
      ...Node.make("b", "b"),
      rPath: [Relations.RIGHT],
    }),
    [||],
  )
);

let testSelection2() = testSelection(
  Bbox.makeCoord(0., 0.),
  Bbox.makeCoord(15., 15.),
  n => switch(n.id) {
    | None => Bbox.make(0., 0., 0., 0.)
    | Some(id) => switch(id) {
      | "a" => Bbox.make(0., 0., 10., 10.)
      | "b" => Bbox.make(20., 0., 10., 10.)
      | "c" => Bbox.make(10., 0., 10., 10.)
      | "d" => Bbox.make(20., 0., 10., 10.)
      | _ => Bbox.make(0., 0., 0., 0.)
    }
  },
  abcd,
  (
    Some(Node.make("a", "a")),
    [|
      {
        ...Node.make("c", "c"),
        rPath: [Relations.SUPER, Relations.RIGHT],
      },
    |],
  )
);

let testSelection3() = testSelection(
  Bbox.makeCoord(0., 0.),
  Bbox.makeCoord(15., 15.),
  n => switch(n.id) {
    | None => Bbox.make(0., 0., 0., 0.)
    | Some(id) => switch(id) {
      | "a" => Bbox.make(0., 0., 10., 10.)
      | "b" => Bbox.make(10., 0., 10., 10.)
      | "c" => Bbox.make(20., 0., 10., 10.)
      | "d" => Bbox.make(10., 0., 10., 10.)
      | _ => Bbox.make(0., 0., 0., 0.)
    }
  },
  Node.make("a", "a")
  |> Node.relate(Relations.RIGHT, Node.make("b", "b")
    |> Node.relate(Relations.SUPER, Node.make("c", "c")
      |> Node.relate(Relations.SUPER, Node.make("d", "d")
        |> Node.relate(Relations.SUB, Node.make("e", "e"))
      )
    )
  ),
  (
    Some(
      Node.make("a", "a")
      |> Node.relate(Relations.RIGHT, Node.make("b", "b"))
    ),
    [|
      {
        ...Node.make("d", "d"),
        rPath: [Relations.SUPER, Relations.SUPER, Relations.RIGHT],
      }
      |> Node.relate(Relations.SUB, Node.make("e", "e"))
    |],
  )
);

let testSelection4() = testSelection(
  Bbox.makeCoord(100., 0.),
  Bbox.makeCoord(100., 5.),
  getBox,
  abcd,
  (
    None,
    [||],
  )
);

let testSelection5() = testSelection(
  Bbox.makeCoord(140., 0.),
  Bbox.makeCoord(100., 30.),
  n => switch(n.id) {
    | None => Bbox.make(0., 0., 0., 0.)
    | Some(id) => switch(id) {
      | "a" => Bbox.make(0., 10., 10., 10.)
      | "b" => Bbox.make(110., 10., 10., 10.)
      | "c" => Bbox.make(130., 10., 10., 10.)
      | "d" => Bbox.make(0., 10., 10., 10.)
      | _ => Bbox.make(0., 0., 0., 0.)
    }
  },
  abcd,
  (
    Some(
      {
        ...Node.make("b", "b"),
        rPath: [Relations.RIGHT],
      }
      |> Node.relate(Relations.SUPER, Node.make("c", "c"))
    ),
    [||],
  )
);

let testSelection6() = testSelection(
  Bbox.makeCoord(160., 0.),
  Bbox.makeCoord(100., 30.),
  n => switch(n.id) {
    | None => Bbox.make(0., 0., 0., 0.)
    | Some(id) => switch(id) {
      | "a" => Bbox.make(0., 10., 10., 10.)
      | "b" => Bbox.make(110., 10., 10., 10.)
      | "c" => Bbox.make(130., 10., 10., 10.)
      | "d" => Bbox.make(150., 10., 10., 10.)
      | "e" => Bbox.make(0., 10., 10., 10.)
      | _ => Bbox.make(0., 0., 0., 0.)
    }
  },
  abcd,
  (
    Some(
      {
        ...Node.make("b", "b"),
        rPath: [Relations.RIGHT],
      }
      |> Node.relate(Relations.SUPER, Node.make("c", "c")
        |> Node.relate(Relations.SUPER, Node.make("d", "d"))
      )
    ),
    [||],
  )
);

let rec makeIRights(i) = {
  let base = Node.make(i->string_of_int, i->string_of_int);
  if ( i > 1 ) {
    base
    |> Node.relate(Relations.RIGHT, makeIRights(i-1))
  } else {
    base
  }
};

let testSelectionStress(i) = () => testSelection(
  Bbox.makeCoord(0., 0.),
  Bbox.makeCoord(i->float_of_int +. 5., i->float_of_int +. 5.),
  n => {
    let f = switch(n.id){
      | None => 0.
      | Some(id) => id->int_of_string->float_of_int
    };
    Bbox.make(f, f, 1., 1.)
  },
  makeIRights(i),
  (
    Some(makeIRights(i)),
    [||],
  )
);


let runSelection() = {
  [
    testSelection0,
    testSelection1,
    testSelection2,
    testSelection3,
    testSelection4,
    testSelection5,
    testSelection6,
    testSelectionStress(10),
    testSelectionStress(100),
    // testSelectionStress(1000),
  ]
  |> TestUtil.runTests("Selection");
};

let testGetSelectionBox(selection, getBox, r) = TestUtil.test(
  () => Selection.getSelectionBox(selection, getBox),
  r,
);

let testGetSelectionBox0() = testGetSelectionBox(
  (
    None,
    [||]
  ),
  getBox,
  Bbox.make(0., 0., 0., 0.)
);

let testGetSelectionBox1() = testGetSelectionBox(
  (
    Some(abc),
    [||]
  ),
  getBox,
  Bbox.make(0., 0., 40., 10.)
);

let runGetSelectionBox() = {
  [
    testGetSelectionBox0,
    testGetSelectionBox1,
  ]
  |> TestUtil.runTests("Get Selection Box");
};


let run() = {
  runRect();
  runInBox();
  runClosest();
  runSelectionSubtrees();
  runSelection();
  runGetSelectionBox();
};